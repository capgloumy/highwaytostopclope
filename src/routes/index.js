
import Vue from 'vue'
import Router from 'vue-router'
import Master from "../components/Master";
import Conseil from "../components/Conseil";
import Signin from "../components/Signin";
import Signup from "../components/Signup";
import Apropos from "../components/Apropos";
import Stats from "../components/Stats";
import Stats_detail from "../components/Stats_detail";
import Exercices from "../components/Exercices";
import Exo_detail from "../components/Exo_detail";
import Dependance from "../components/Dependance";
import * as firebase from "firebase";

Vue.use(Router);

const router = new Router({
    mode: "history",
    routes: [
        {
            path: '*',
            redirect: '/signin'
        },
        {
            path: '/signin',
            name: 'signin',
            component: Signin,
        },
        {
            path: '/signup',
            name: 'signup',
            component: Signup,
        },
        {
            path: '/main',
            name: 'master',
            component: Master,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/conseil',
            name: 'conseil',
            component: Conseil,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/apropos',
            name: 'apropos',
            component: Apropos,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/stats',
            name: 'stats',
            component: Stats,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/details/:id',
            name: 'details',
            component: Stats_detail,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/exercices',
            name: 'exercices',
            component: Exercices,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/exo/:id',
            name: 'exo_detail',
            component: Exo_detail,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/dependance',
            name: 'dependance',
            component: Dependance,
            meta: {
                requiresAuth: true
            }
        }
    ]
});

router.beforeEach((to, from, next) => {
    const requiresAuth = to.matched.some(x => x.meta.requiresAuth)
    const currentUser = firebase.auth().currentUser

    if (requiresAuth && !currentUser) {
        next('/signin')
    } else if (requiresAuth && currentUser) {
        next()
    } else {
        next()
    }
})

export default router;