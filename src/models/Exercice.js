
export default class Exercice {



    constructor(title,exercice, time) {
        this.title=title;
        this.exo=exercice;
        this.time=time
    }

    return_duree(){
        switch(this.time){
            case 1:
                return "moins de 1h";
            case 2:
                return "entre 1h et 2h";
            case 3:
                return "plus de 2h";
        }
    }

}