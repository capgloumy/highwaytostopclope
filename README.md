# Projet pour le cours Technologies Web Pour l'Innovation
## Réalisé par Guillaume Toussaint, Mathieu Leclaire, Clément Nicaudie et Morgane Coltel

### Procédure d'installation du projet

Avoir installé le gestionnaire de paquet npm ainsi que Node.js
Vous devrez également installer le CLI de Vue.js : `npm install -g @vue/cli` sous Windows et `sudo npm install -g @vue/cli`sous linux ou MacOS

Pour récupérer le projet depuis GitLab il faut exécuter :
`git clone https://gitlab.com/capgloumy/highwaytostopclope.git`

Sinon, il n'est pas nécessaire d'effectuer cette étape.

Ensuite, il faut installer les dépendances nécessaires à l'exécution du projet :
`cd highwaytostopclope && npm install`

Pour construire le build de production :
`npm run build`

Et enfin, pour exécuter le projet :
`npm run serve`

Le projet sera désormais accessible sur l'adresse localhost:8080, depuis votre navigateur. Il est important de disposer d'une connexion internet, car l'application intéragit avec une base de données Firebase.